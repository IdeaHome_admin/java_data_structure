package 算法复习课.手动造轮子.common;

/**
 * @author linhao
 * @date created in 3:24 下午 2020/10/20
 */
public interface Iterator<E> {

    boolean hasNext();

    E remove();

    E next();
}
