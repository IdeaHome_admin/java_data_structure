package 算法复习课.手动造轮子.bitmap;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;

/**
 * @author linhao
 * @date created in 11:05 上午 2020/10/25
 */
public class GuavaBloomFilter {

    public static void main(String[] args) {
        BloomFilter<Integer> bloomFilter = BloomFilter.create(
                Funnels.integerFunnel(),
                500,
                0.01
        );
        bloomFilter.put(1);
        bloomFilter.put(2);
        bloomFilter.put(3);
        bloomFilter.put(4);
        bloomFilter.put(5);

        System.out.println(bloomFilter.mightContain(1));
        System.out.println(bloomFilter.mightContain(2));
        System.out.println(bloomFilter.mightContain(1000));
    }
}
