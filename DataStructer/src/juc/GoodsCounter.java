package juc;

import java.util.concurrent.CountDownLatch;

/**
 * @Author linhao
 * @Date created in 11:25 上午 2021/3/28
 */
public class GoodsCounter extends SynchronizedWarrper {

    private int count;

    public GoodsCounter(Integer count) {
        super(count);
        this.count = count;
    }

    public boolean decreaseSync() {
        return super.decrease();
    }

    public boolean decreaseNotSync() {
        count--;
        return true;
    }

    public int getCount() {
        return count;
    }

    public int getCountSync() {
        return (int) super.getValue();
    }

    public static void main(String[] args) throws InterruptedException {
        GoodsCounter goodsCounter = new GoodsCounter(1000);
        goodsCounter.decreaseNotSync();

//        for (int j = 0; j < 1000; j++) {
//            CountDownLatch countDownLatch = new CountDownLatch(1);
//            GoodsCounter goodsCounter = new GoodsCounter(1000);
//            for (int i = 0; i < 1000; i++) {
//                Thread thread = new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            countDownLatch.await();
//                            goodsCounter.decreaseSync();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
//                thread.start();
//            }
//            countDownLatch.countDown();
//            while (Thread.activeCount() > 2) {
//                Thread.yield();
//            }
//            if(goodsCounter.getCountSync()>0){
//                System.out.println("第" + j + "次测试结果异常，最终剩余数目：" + goodsCounter.getCountSync());
//            }
//        }
//        System.out.println("测试结束");

    }
}
