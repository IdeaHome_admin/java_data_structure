package juc;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @Author linhao
 * @Date created in 2:44 下午 2021/3/28
 */
public class CopyOnWriteArraySetDemo {

    public static void main(String[] args) {
        CopyOnWriteArraySet copyOnWriteArraySet = new CopyOnWriteArraySet();
        //写入数据的时候需要拷贝数组，性能开销较大
        copyOnWriteArraySet.add(1);
        //读取数据的时候比较简单 不需要加锁
        copyOnWriteArraySet.contains(1);
        //同样的设计，抽象了一个同步的父类，在这个父类中实现同步的处理逻辑
        Set<Integer> set = Collections.synchronizedSet(new HashSet<>());
        set.add(1);
    }
}
