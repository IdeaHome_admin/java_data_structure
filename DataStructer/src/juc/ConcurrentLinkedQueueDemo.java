package juc;

import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * 线程安全队列
 *
 * @Author linhao
 * @Date created in 3:13 下午 2021/3/28
 */
public class ConcurrentLinkedQueueDemo {

    public static void main(String[] args) {
        //这是一种无锁的高性能队列
        ConcurrentLinkedQueue<Integer> concurrentLinkedQueue = new ConcurrentLinkedQueue();
        concurrentLinkedQueue.add(1);
        concurrentLinkedQueue.add(2);
        concurrentLinkedQueue.add(3);
        concurrentLinkedQueue.offer(1);
        //先进先出
        System.out.println(concurrentLinkedQueue.size());
        Iterator<Integer> iterator = concurrentLinkedQueue.iterator();
        while (iterator.hasNext()){
            Integer result = iterator.next();
            System.out.println(result);
        }
    }
}
