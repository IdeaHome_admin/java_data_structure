package 算法部分.移动数字0;

class Solution {

    public void moveZeroes(int[] nums) {
        int len = nums.length;
        int i=0;
        for(int j =0;j<len;j++){
            if(nums[j]!=0){
                int temp = nums[j];
                nums[j]=nums[i];
                nums[i]=temp;
                i++;
            }
        }
        for (int num : nums) {
            System.out.println(num);
        }
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.moveZeroes(new int[]{0,1,0,3,12});
    }
}