package 算法部分.数组;

import java.util.Arrays;

/**
 * @author linhao
 * @date created in 10:40 下午 2020/10/18
 */
public class Demo2 {

    static int[] invertArr(int[] arr){
        if(arr.length==1){
            return arr;
        }
        int p[] = new int[arr.length];
        for(int j=arr.length-1,i=0;j>=0;j--,i++){
            p[i]=arr[j];
        }
        return p;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{1, 2, 3, 4, 5, 6, 7,8};
        arr = invertArr(arr);
        System.out.println(Arrays.toString(arr));
    }
}
