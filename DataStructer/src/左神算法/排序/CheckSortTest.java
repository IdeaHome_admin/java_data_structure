package 左神算法.排序;

import com.sun.xml.internal.bind.v2.model.annotation.Quick;

import java.util.Arrays;
import java.util.Random;

/**
 * @Author linhao
 * @Date created in 8:46 下午 2022/1/28
 */
public class CheckSortTest {

    static int[] generateRandomArray() {
        Random r = new Random();
        int[] arr = new int[10000];
        for (int j = 0; j < arr.length; j++) {
            arr[j] = r.nextInt(10000);
        }
        return arr;
    }

    public static void main(String[] args) {
        int[] arr = generateRandomArray();
        int[] arr2 = new int[arr.length];
        System.arraycopy(arr, 0, arr2, 0, arr.length);
        Arrays.sort(arr);
//        InsertSort.sort3(arr2);
//        ShellSort.sortV3(arr2);
        QuickSort.sort2(arr2, 0, arr2.length-1);
        boolean isSame = true;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != arr2[i]) {
                isSame = false;
            }
        }
        System.out.println("排序算法验证结果：" + isSame);
    }
}
