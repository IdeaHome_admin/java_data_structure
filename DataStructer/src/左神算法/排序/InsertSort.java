package 左神算法.排序;

/**
 * 基本有序的排序，稳定 比较推荐使用的一种排序
 *
 * @Author linhao
 * @Date created in 10:17 上午 2022/1/24
 */
public class InsertSort {

    public static int[] arr = {122, 32, 43, 12, 55, 77, 45, 83};

    //从数组的头部开始，将小的元素往前换
    public static void sort(int[] arr) {
        for (int j = 0; j < arr.length; j++) {
            for (int k = j; k > 0; k--) {
                if (arr[k] < arr[k - 1]) {
                    swap(arr, k, k - 1);
                }
            }
        }
    }


    public static void sort2(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int k = i; k > 0; k--) {
                if (arr[k] < arr[k - 1]) {
                    swap(arr, k, k - 1);
                }
            }
        }
    }

    public static void sort3(int[] arr) {
        for (int j = 0; j < arr.length; j++) {
            for (int k = j; k > 0; k--) {
                if (arr[k] < arr[k - 1]) {
                    swap(arr,k,k-1);
                }
            }
        }
    }

    public static void swap(int[] arr, int j, int p) {
        int temp = arr[j];
        arr[j] = arr[p];
        arr[p] = temp;
    }


    public static void displayArr() {
        for (Integer integer : arr) {
            System.out.print(integer + " ");
        }
        System.out.println("");
    }

    public static void main(String[] args) {
        InsertSort.sort3(arr);
        InsertSort.displayArr();
    }
}
