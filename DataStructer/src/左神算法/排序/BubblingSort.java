package 左神算法.排序;

/**
 * 冒泡排序
 *
 * @Author linhao
 * @Date created in 11:16 下午 2022/1/23
 */
public class BubblingSort {

    public static int[] arr = new int[]{99, 2, 43, 123, 43, 7, 4, 0};

    public static void sort(int[] arr) {
        for (int j = arr.length; j > 0; j--) {
            for (int i = 0; i < j - 1; i++) {
                if (arr[i] > arr[i + 1]) {
                    swap(arr, i, i + 1);
                }
            }
        }
    }

    public static void sort2(int[] arr) {
        for (int k = arr.length - 1; k > 0; k--) {
            for (int j = 0; j < k; j++) {
                if (arr[j] > arr[j + 1]) {
                    swap(arr, j, j + 1);
                }
            }
        }
    }

    public static void sort3(int[] arr) {
        for (int i = arr.length - 1; i > 0; i--) {
            for (int k = 0; k < i; k++) {
                if (arr[k] > arr[k + 1]) {
                    swap(arr, k, k + 1);
                }
            }
        }
    }

    public static void swap(int[] arr, int j, int p) {
        int temp = arr[j];
        arr[j] = arr[p];
        arr[p] = temp;
    }


    public static void display(int[] arr) {
        for (int i : arr) {
            System.out.print(i + " ");
        }
    }

    public static void main(String[] args) {
        BubblingSort.sort3(arr);
        BubblingSort.display(arr);
    }
}
