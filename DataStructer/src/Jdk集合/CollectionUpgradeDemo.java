package Jdk集合;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 关于集合在优化过程中的案例Demo
 *
 * @Author linhao
 * @Date created in 6:19 下午 2022/1/23
 */
public class CollectionUpgradeDemo {

    class User{
        int id;
        String name;

        public User(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public void testStreamMapDemo(){
        List<User> userList = new ArrayList<>();
        userList.add(new User(1,"idea"));
        userList.add(new User(2,"idea2"));
        userList.add(new User(3,null));
        //这里面会对null数据抛出异常
        Map<Integer,String> map = userList.stream().collect(Collectors.toMap(User::getId,User::getName));
        System.out.println(map);
    }

    public static void main(String[] args) {
        CollectionUpgradeDemo collectionUpgradeDemo = new CollectionUpgradeDemo();
        collectionUpgradeDemo.testStreamMapDemo();
    }
}
